#%% SECTION 1
#---------------------------------------------[Imports]------------------------
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import matplotlib.tri as tri
import os
import sys
import numpy as np
import plotter as pl

#----------------------------------------------[Plot Style]--------------------
# plt.style.use('paper-AIAA-2') # Proper Formatting
# plt.style.use('thesis-style-1')
#------------------------------------------------------------------------------

#----------------------------------------------[Plot Style]--------------------
plt.style.use('contour-style') # Peoper Formatting
#------------------------------------------------------------------------------

#----------------------------------------------[Setting PYSPOS]----------------
# Current, parent and file paths
CWD = os.getcwd()

# Import library specific modules
sys.path.append("../../../")
from pyspod.spod_low_storage import SPOD_low_storage
from pyspod.spod_low_ram     import SPOD_low_ram
from pyspod.spod_streaming   import SPOD_streaming
#------------------------------------------------------------------------------

#---------------------------------------------[Create Result Folder]-----------
# if os.path.isdir('./Plots') == False:
#     os.mkdir('./Plots')
#------------------------------------------------------------------------------

#----------------------------------------------[Create Data]-------------------
#----------------------------------------------[Read Data]---------------------
p = np.load('./NPY Results/Qt.npy', mmap_mode='r')
# variables = ['q']
#------------------------------------------------------------------------------

# Let's take a look at the dimension of our data
# to make sure it is compatible with pyspod
print('p.shape = ', p.shape)
#------------------------------------------------------------------------------

#%% SECTION 2

#----------------------------------------------[Input to PYSPOD]---------------
# Let's define the required parameters into a dictionary
params = dict()

params['nt'   ] = 10000              # number of time-frames
params['xdim'   ] = 2                # number of spatial dimensions
params['nv'   ] = 1                  # number of variables
params['dt'   ] = 2e-4               # time-step

size = 512
params['n_FFT'   ] = size # 100 : number of frequency bins ?
params['n_overlap' ] = size/2
params['mean'] = 'blockwise'

# -- required parameters
# params['time_step'   ] = 1                	# data time-sampling
# params['n_snapshots' ] = t.shape[0]       	# number of time snapshots (we consider all data)
# params['n_space_dims'] = 2                	# number of spatial dimensions (longitude and latitude)
# params['n_variables' ] = len(variables)     # number of variables
# params['n_DFT'       ] = 100          		# length of FFT blocks (100 time-snapshots)

# -- optional parameters
# params['overlap'          ] = 0 			# dimension block overlap region
# params['mean_type'        ] = 'blockwise' 	# type of mean to subtract to the data
params['normalize_weights'] = False        	# normalization of weights by data variance
params['normalize_data'   ] = False   		# normalize data by data variance
params['n_modes_save'     ] = 3      		# modes to be saved
params['conf_level'       ] = 0.95   		# calculate confidence level
params['reuse_blocks'     ] = False 		# whether to reuse blocks if present
params['savefft'          ] = True   		# save FFT blocks to reuse them in the future (saves time)
params['savedir'          ] = os.path.join(CWD, 'SPOD') # folder where to save results
#------------------------------------------------------------------------------

#----------------------------------------------[PYSPOD Analysis]---------------
# Initialize libraries for the low_storage algorithm
spod_ram = SPOD_low_ram(p, params=params, data_handler=False, variables=['q'])
spod_ram.fit()

# Let's plot the data
# spod_ls.plot_2D_data(time_idx=[1,2])
# spod_ls.plot_data_tracers(coords_list=[(5,2.5)], time_limits=[0,t.shape[0]])
# spod_ls.generate_2D_data_video(sampling=10, time_limits=[0,t.shape[0]])
#------------------------------------------------------------------------------

#%% SECTION 3

#----------------------------------------------[Create Folder]-----------------
if os.path.isdir('./SPOD/nfft'+str(size)+'-data') == False:
    os.mkdir('./SPOD/nfft'+str(size)+'-data')
#------------------------------------------------------------------------------

#----------------------------------------------[Save Mode Energy]--------------
# Select the frequency bin for the fi
freq = spod_ram.freq
eigs = spod_ram.eigs

np.save('./SPOD/nfft'+str(size)+'-data/freq.npy', freq)
np.save('./SPOD/nfft'+str(size)+'-data/eigs.npy', eigs)
#------------------------------------------------------------------------------

#%% SECTION 4

#----------------------------------------------[Create Folder]-----------------
if os.path.isdir('./SPOD/nfft'+str(size)+'-plots') == False:
    os.mkdir('./SPOD/nfft'+str(size)+'-plots')
#------------------------------------------------------------------------------

#----------------------------------------------[Frequency f0]------------------
f0 = 205 # frequency idex = ?
f0_found, f0_idx = spod_ram.find_nearest_freq(freq_required=f0, freq=freq)
modes_at_f0 = spod_ram.get_modes_at_freq(freq_idx=f0_idx)
m0_f0 = np.real(modes_at_f0[:,:,0,0])

# save data
np.save('./SPOD/nfft'+str(size)+'-data/m0_f0.npy', m0_f0)
print("\nfor fi = ", f0, "freq_found = ", f0_found, "and freq_idx = ", f0_idx, "\n")

# mode plot
pl.contour_plotter(1, 'mode0_f0', m0_f0, 20, 'RdBu', '%1.4f', 
                   './SPOD/nfft'+str(size)+'-plots/')
#------------------------------------------------------------------------------

#----------------------------------------------[Frequency f1]------------------
f1 = 2*205 # frequency idex = ?
f1_found, f1_idx = spod_ram.find_nearest_freq(freq_required=f1, freq=freq)
modes_at_f1 = spod_ram.get_modes_at_freq(freq_idx=f1_idx)
m0_f1 = np.real(modes_at_f1[:,:,0,0])

# save data
np.save('./SPOD/nfft'+str(size)+'-data/m0_f1.npy', m0_f1)
print("\nfor fi = ", f1, "freq_found = ", f1_found, "and freq_idx = ", f1_idx, "\n")

# mode plot
pl.contour_plotter(1, 'mode0_f1', m0_f1, 20, 'RdBu', '%1.4f', 
                    './SPOD/nfft'+str(size)+'-plots/')
#------------------------------------------------------------------------------



