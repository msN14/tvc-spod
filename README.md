# TVC SPOD (Spectral Proper Orthogonal Decomposition)

This code uses the time snapshot data of 2D images in NPY (numpy) format and decompose using spectral proper orthogonal decomposition algorithm. It uses the project [PySPOD](https://github.com/MathEXLab/PySPOD).

The code is developed to study the TVC combustion process using OH* chemiluminescence data. The major modes of oscillations are captured to find the imortant modifications to mitigate the oscillations by controlling the variables or geometrical modifications.

1. The input numpy array (.npy) is arranged as 3D array with snapshots of 2D arrays in the 3rd direction

2. provide the number of time frames, spatial dimensions, number of variables and time step size (resolution of the measurement)

3. FFT parameters, block size, overlap ratio, number modes to solve and confidence level of the calculation

