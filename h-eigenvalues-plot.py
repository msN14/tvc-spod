#%% SECTION 1
#----------------------------------------------[Imports]-----------------------
# import ExpObject as eo

import matplotlib.pyplot as plt
import numpy as np
import os
#------------------------------------------------------------------------------

#----------------------------------------------[Plot Style]--------------------
# plt.style.use('paper-AIAA-2') # Proper Formatting
plt.style.use('thesis-style-1')
#------------------------------------------------------------------------------

#----------------------------------------------[Create Folder]-----------------
size = 512

if os.path.isdir('./SPOD/nfft'+str(size)+'-plots') == False:
    os.mkdir('./SPOD/nfft'+str(size)+'-plots')
#------------------------------------------------------------------------------

#----------------------------------------------[Read Data]---------------------
# Loading data
freq = np.load('./SPOD/nfft'+str(size)+'-data/freq.npy')
eigs = np.load('./SPOD/nfft'+str(size)+'-data/eigs.npy')
#------------------------------------------------------------------------------

#----------------------------------------------[Plot : PSD in Pa^2/Hz]---------
fig, ax1 = plt.subplots(num=1)

ax1.plot(freq, np.real(eigs[:,0]), "k-", linewidth=1, markersize=10, 
          label="Mode 1", markeredgewidth=1, markerfacecolor='white')
ax1.plot(freq, np.real(eigs[:,1]), "k--", linewidth=1, markersize=10, 
          label="Mode 2", markeredgewidth=1, markerfacecolor='white')

# xmax = 1500
# ymax = max(Pxx1)
# ax1.set_xlim(0, xmax)
ax1.set_ylim(1e6,1e12)

ax1.set_xlabel("Frequency [Hz]")
ax1.set_ylabel("Eigenvalues [-]")
# ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.legend()
ax1.grid(True)

# Peak denote in plot
# ax1.axvline(x=190, color='k', linestyle='dashed', linewidth=1)
ax1.text(205, 3*1e11, "f$_0$="+str(205), horizontalalignment='center', 
          verticalalignment='center',fontsize=18, bbox=dict(facecolor='white',
                                    alpha=1, boxstyle='square', ec='w', pad=0))
# f0 = 205
# ax1.text(f0,ymax+2*ymax, "f$_0$="+str(round(f0)), horizontalalignment='center', 
#           verticalalignment='center',fontsize=18, bbox=dict(facecolor='white',
#                                     alpha=1, boxstyle='square', ec='w', pad=0))
# ax1.text(2*f0,7e5, "2f$_0$", horizontalalignment='center', 
#           verticalalignment='center',fontsize=18, bbox=dict(facecolor='white',
#                                     alpha=1, boxstyle='square', ec='w', pad=0))

fig.savefig('./SPOD/nfft'+str(size)+'-plots/eigs-freq.pdf')
fig.savefig('./SPOD/nfft'+str(size)+'-plots/eigs-freq.svg')
fig.savefig('./SPOD/nfft'+str(size)+'-plots/eigs-freq.png')
#------------------------------------------------------------------------------